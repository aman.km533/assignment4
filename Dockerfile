FROM node:14.17.3
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package*.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
EXPOSE 3000
CMD ["node", "src/index.js"] ## Depends on the site that's being deployed
